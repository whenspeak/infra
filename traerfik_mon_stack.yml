version: '3.7'

volumes:
    prometheus_data: {}
    grafana_data: {}
    traefik_data: {}
    loki_data: {}

networks:
  monitor-net:
    driver: overlay
    name: inbound
  traefik:
    driver: overlay
    name: traefik

services:
  traefik:
    image: traefik:v2.2.1
    networks:
      - traefik
      - monitor-net
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - traefik_data:/acme/
      - ./traefik/traefik.yml:/etc/traefik/traefik.yml
    ports:
      - target: 80
        published: 80
        mode: host
      - target: 443
        published: 443
        mode: host
      - target: 3100
        published: 3100
        mode: host
      - target: 8080
        published: 8080
        protocol: tcp
        mode: ingress
    deploy:
      mode: global
      placement:
        constraints:
          - node.role == manager
      update_config:
        parallelism: 1
        delay: 10s
      restart_policy:
        condition: any
      labels:
        - "traefik.http.routers.api.rule=Host(`traefik.admin.whenspeak.ru`)"
        - "traefik.http.routers.api.service=api@internal"
        - "traefik.http.routers.api.middlewares=auth"
        - "traefik.http.middlewares.auth.basicauth.users=admin:$$apr1$$.jRF9DzQ$$tas8oOZUoxTGycFW/sjYg0"
        - "traefik.http.services.dummy-svc.loadbalancer.server.port=9999"
        - "traefik.enable=true"
        - "traefik.http.routers.api.tls=true"
        - "traefik.http.routers.api.tls.certresolver=prodssl"
        - "traefik.http.routers.api.entrypoints=web,websecure"

  prometheus:
    image: prom/prometheus:v2.18.1
    volumes:
      - ./prometheus/:/etc/prometheus/
      - prometheus_data:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.console.libraries=/usr/share/prometheus/console_libraries'
      - '--web.console.templates=/usr/share/prometheus/consoles'
      - '--web.enable-lifecycle'
    networks:
      - monitor-net 
      - traefik    
    deploy:
      placement:
        constraints:
          - node.role==manager
      labels:
        - "traefik.http.routers.prometheus.rule=Host(`mon.admin.whenspeak.ru`)"
        - "traefik.http.routers.prometheus.service=prometheus"
        - "traefik.http.routers.prometheus.middlewares=auth"
        - "traefik.http.middlewares.auth.basicauth.users=admin:$$apr1$$.jRF9DzQ$$tas8oOZUoxTGycFW/sjYg0" 
        - "traefik.http.services.prometheus.loadbalancer.server.port=9090"
        - "traefik.docker.network=traefik"
        - "traefik.enable=true"
        - "traefik.http.routers.prometheus.tls=true"
        - "traefik.http.routers.prometheus.tls.certresolver=prodssl"
        - "traefik.http.routers.prometheus.entrypoints=web,websecure"
      restart_policy:
        condition: on-failure

  node-exporter:
    image: prom/node-exporter:v1.0.0-rc.1
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    hostname: ${HOSTNAME}
    command: 
      - '--path.procfs=/host/proc' 
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.ignored-mount-points'
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    networks:
      - monitor-net
    deploy:
      mode: global
      restart_policy:
        condition: on-failure

  grafana:
    image: grafana/grafana:7.0.0
    depends_on:
      - prometheus
    volumes:
      - grafana_data:/var/lib/grafana
      - ./grafana/provisioning/:/etc/grafana/provisioning/
    env_file:
      - ./grafana/config.monitoring
    networks:
      - monitor-net
      - traefik
    user: "472"
    deploy:
      placement:
        constraints:
          - node.role==manager
      labels:
        - "traefik.http.routers.grafana.rule=Host(`grafana.admin.whenspeak.ru`)"
        - "traefik.http.routers.grafana.service=grafana"
        - "traefik.http.services.grafana.loadbalancer.server.port=3000"
        - "traefik.docker.network=traefik"
        - "traefik.enable=true"
        - "traefik.http.routers.grafana.tls=true"
        - "traefik.http.routers.grafana.tls.certresolver=prodssl"
        - "traefik.http.routers.grafana.entrypoints=web,websecure"
      restart_policy:
        condition: on-failure
  
  renderer:
    image: grafana/grafana-image-renderer:2.0.0
    networks:
      - monitor-net
    environment:
      ENABLE_METRICS: 'true'
    deploy:
      restart_policy:
        condition: on-failure

  loki:
    image: grafana/loki:1.5.0
    networks:
      - monitor-net
      - traefik
    command: -config.file=/etc/loki/config.yaml
    volumes:
      - ./loki/config.yml:/etc/loki/config.yaml
      - loki_data:/loki
    deploy:
      placement:
        constraints:
          - node.role==manager
      restart_policy:
        condition: on-failure
      labels:
        - "traefik.http.routers.loki.rule=Host(`loki.admin.whenspeak.ru`)"
        - "traefik.http.routers.loki.service=loki"
        - "traefik.http.services.loki.loadbalancer.server.port=3100"
        - "traefik.docker.network=traefik"
        - "traefik.enable=true"
        - "traefik.http.routers.loki.entrypoints=loki"

  cadvisor:
    image: gcr.io/google-containers/cadvisor:v0.36.0
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
    ports:
      - 8088:8080
    networks:
      - monitor-net
    deploy:
      mode: global
      restart_policy:
        condition: any

  # alertmanager:
  #   image: prom/alertmanager
  #   ports:
  #     - 9093:9093
  #   volumes:
  #     - "./alertmanager/:/etc/alertmanager/"
  #   networks:
  #     - monitor-net
  #   command:
  #     - '--config.file=/etc/alertmanager/config.yml'
  #     - '--storage.path=/alertmanager'
  #   deploy:
  #     placement:
  #       constraints:
  #          - node.role==manager
  #     restart_policy:
  #       condition: any  